FROM alpine:3 as build

ENV container docker

WORKDIR /work

RUN apk add --no-cache \
      bash~=5 \
      curl~=7 \
      upx~=3 \
    && DOTENV_LINTER_RELEASES="https://github.com/dotenv-linter/dotenv-linter/releases" \
    && DOWNLOAD_URL="${DOTENV_LINTER_RELEASES}/latest/download/dotenv-linter-alpine-x86_64.tar.gz" \
    && curl -sSfL "$DOWNLOAD_URL" -o "dotenv-linter.tar.gz" \
    && tar -xzvf dotenv-linter.tar.gz \
    && upx dotenv-linter

FROM alpine:3

WORKDIR /work

COPY --from=build /work/dotenv-linter /usr/local/bin/dotenv-linter

ENTRYPOINT ["dotenv-linter"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/dotenv-linter/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/dotenv-linter.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
